package ru.pkonovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.util.TerminalUtil;

public final class TaskByNameStartCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Start task by name";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-start-by-name";
    }

    @Override
    public void execute() {
        System.out.println("[START TASK]");
        System.out.println("ENTER NAME:");
        endpointLocator.getTaskEndpoint().startTaskByName(endpointLocator.getSession(), TerminalUtil.nextLine());
    }

}
