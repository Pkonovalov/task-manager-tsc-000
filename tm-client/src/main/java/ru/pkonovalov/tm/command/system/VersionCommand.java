package ru.pkonovalov.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.pkonovalov.tm.command.AbstractCommand;

public final class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String commandArg() {
        return "-v";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show version";
    }

    @NotNull
    @Override
    public String commandName() {
        return "version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(endpointLocator.getPropertyService().getApplicationVersion());
        System.out.println("[BUILD]");
        System.out.println(Manifests.read("build"));
    }

}
