package ru.pkonovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.util.TerminalUtil;

public final class TaskByIdFinishCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Finish task by id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-finish-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[FINISH TASK]");
        System.out.println("ENTER ID:");
        endpointLocator.getTaskEndpoint().finishTaskById(endpointLocator.getSession(), TerminalUtil.nextLine());
    }

}
