package ru.pkonovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.util.TerminalUtil;

public final class TaskByIndexRemoveCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Remove task by index";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-remove-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK]");
        System.out.println("ENTER INDEX:");
        endpointLocator.getTaskEndpoint().removeTaskByIndex(endpointLocator.getSession(), TerminalUtil.nextNumber());
    }

}
