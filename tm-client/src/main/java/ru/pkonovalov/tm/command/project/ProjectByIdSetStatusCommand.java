package ru.pkonovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.endpoint.Status;
import ru.pkonovalov.tm.exception.entity.ProjectNotFoundException;
import ru.pkonovalov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectByIdSetStatusCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Set project status by id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-set-status-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[SET PROJECT STATUS]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        if (!endpointLocator.getProjectEndpoint().existsProjectById(endpointLocator.getSession(), id))
            throw new ProjectNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        endpointLocator.getProjectEndpoint().setProjectStatusById(endpointLocator.getSession(), id, Status.valueOf(TerminalUtil.nextLine()));
    }

}
