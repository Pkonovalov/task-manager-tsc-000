package ru.pkonovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.exception.entity.TaskNotFoundException;
import ru.pkonovalov.tm.util.TerminalUtil;

public final class TaskByProjectIdBindCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Bind task by project id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-bind";
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT BY ID]");
        if (endpointLocator.getTaskEndpoint().countTask(endpointLocator.getSession()) < 1)
            throw new TaskNotFoundException();
        System.out.println("ENTER TASK INDEX:");
        @Nullable final String taskId = endpointLocator.getTaskEndpoint().getTaskIdByIndex(endpointLocator.getSession(), TerminalUtil.nextNumber());
        if (taskId == null) return;
        System.out.println("ENTER PROJECT ID:");
        endpointLocator.getTaskEndpoint().bindTaskById(endpointLocator.getSession(), TerminalUtil.nextLine(), taskId);
    }

}
