package ru.pkonovalov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.command.AbstractCommand;

public final class ExitCommand extends AbstractCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Close application";
    }

    @NotNull
    @Override
    public String commandName() {
        return "exit";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
