package ru.pkonovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.util.TerminalUtil;

public final class ProjectByIdRemoveCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Remove project by id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-remove-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER ID:");
        endpointLocator.getProjectEndpoint().removeProjectById(endpointLocator.getSession(), TerminalUtil.nextLine());
    }

}
