package ru.pkonovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.util.TerminalUtil;

public final class ProjectByIndexViewCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "View project by index";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-view-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        showProject(endpointLocator.getProjectEndpoint().findProjectByIndex(endpointLocator.getSession(), TerminalUtil.nextNumber()));
    }

}
