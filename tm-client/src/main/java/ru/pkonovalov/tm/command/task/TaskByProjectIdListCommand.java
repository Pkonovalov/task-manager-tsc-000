package ru.pkonovalov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.endpoint.Task;
import ru.pkonovalov.tm.util.TerminalUtil;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public final class TaskByProjectIdListCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show task list by project id";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-list-by-project-id";
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final List<Task> taskList = endpointLocator.getTaskEndpoint().findTaskByProjectId(endpointLocator.getSession(), TerminalUtil.nextLine());
        System.out.printf("\t| %-36s | %-12s | %-20s | %-30s | %-30s | %-30s | %s |%n", "ID", "STATUS", "NAME", "CREATED", "STARTED", "FINISHED", "PROJECT");
        if (taskList == null) return;
        @NotNull AtomicInteger index = new AtomicInteger(1);
        taskList.forEach(e -> System.out.println(index.getAndIncrement() + ".\t" + e));
    }

}
