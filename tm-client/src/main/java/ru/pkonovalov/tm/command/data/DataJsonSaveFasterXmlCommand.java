package ru.pkonovalov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.command.AbstractCommand;

public final class DataJsonSaveFasterXmlCommand extends AbstractCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @Nullable
    @Override
    public String commandDescription() {
        return "Save data to json file";
    }

    @NotNull
    @Override
    public String commandName() {
        return "data-json-save";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON SAVE]");
        endpointLocator.getAdminEndpoint().saveJson(endpointLocator.getSession());
    }

}
