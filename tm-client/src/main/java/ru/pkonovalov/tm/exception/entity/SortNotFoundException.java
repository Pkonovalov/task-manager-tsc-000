package ru.pkonovalov.tm.exception.entity;

import ru.pkonovalov.tm.exception.AbstractException;

public class SortNotFoundException extends AbstractException {

    public SortNotFoundException() {
        super("Error! Sort not found...");
    }

}
