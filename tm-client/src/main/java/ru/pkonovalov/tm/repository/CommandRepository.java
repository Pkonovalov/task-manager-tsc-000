package ru.pkonovalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.ICommandRepository;
import ru.pkonovalov.tm.command.AbstractCommand;

import java.util.*;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> arguments = new TreeMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands = new TreeMap<>((o1, o2) -> {
        if (o1.contains("-") && !o2.contains("-")) return -1;
        if (o2.contains("-") && !o1.contains("-")) return 1;
        return o1.compareTo(o2);
    });

    @Override
    public void add(@NotNull final AbstractCommand command) {
        @Nullable final String arg = command.commandArg();
        @NotNull final String name = command.commandName();
        if (arg != null) arguments.put(arg, command);
        commands.put(name, command);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArgsCommands() {
        @NotNull final List<AbstractCommand> result = new ArrayList<>();
        commands.values().stream()
                .filter(e -> !isEmpty(e.commandArg()))
                .forEach(result::add);
        return result;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public Collection<String> getCommandArgs() {
        @NotNull final List<String> result = new ArrayList<>();
        commands.values().stream()
                .filter(e -> !isEmpty(e.commandArg()))
                .forEach(e -> result.add(e.commandArg()));
        return result;
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArg(@NotNull final String name) {
        return arguments.get(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @NotNull
    @Override
    public Collection<String> getCommandNames() {
        @NotNull final List<String> result = new ArrayList<>();
        commands.values().stream()
                .filter(e -> !isEmpty(e.commandName()))
                .forEach(e -> result.add(e.commandName()));
        return result;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

}
