package ru.pkonovalov.tm.api;

import ru.pkonovalov.tm.endpoint.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
