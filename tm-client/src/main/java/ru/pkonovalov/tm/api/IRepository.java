package ru.pkonovalov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.endpoint.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(@Nullable E entity);

    void addAll(@Nullable List<E> list);

    void clear(@NotNull String userId);

    void clear();

    boolean existsById(@NotNull String userId, @NotNull String id);

    @Nullable
    List<E> findAll(@NotNull String userId, @NotNull Comparator<E> comparator);

    @Nullable
    List<E> findAll(@NotNull String userId);

    @Nullable
    List<E> findAll();

    @Nullable
    E findById(@NotNull String userId, @NotNull String id);

    void remove(@NotNull E entity);

    void removeById(@NotNull String userId, @NotNull String id);

    int size();

    int size(@NotNull String userId);

}
