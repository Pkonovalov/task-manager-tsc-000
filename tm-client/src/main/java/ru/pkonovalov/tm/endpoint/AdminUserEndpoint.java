package ru.pkonovalov.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.4.2
 * 2021-03-12T16:43:55.691+03:00
 * Generated source version: 3.4.2
 */
@WebService(targetNamespace = "http://endpoint.tm.pkonovalov.ru/", name = "AdminUserEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface AdminUserEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.pkonovalov.ru/AdminUserEndpoint/lockUserByLoginRequest", output = "http://endpoint.tm.pkonovalov.ru/AdminUserEndpoint/lockUserByLoginResponse")
    @RequestWrapper(localName = "lockUserByLogin", targetNamespace = "http://endpoint.tm.pkonovalov.ru/", className = "ru.pkonovalov.tm.endpoint.LockUserByLogin")
    @ResponseWrapper(localName = "lockUserByLoginResponse", targetNamespace = "http://endpoint.tm.pkonovalov.ru/", className = "ru.pkonovalov.tm.endpoint.LockUserByLoginResponse")
    public void lockUserByLogin(

            @WebParam(name = "session", targetNamespace = "")
                    ru.pkonovalov.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.pkonovalov.ru/AdminUserEndpoint/removeUserByLoginRequest", output = "http://endpoint.tm.pkonovalov.ru/AdminUserEndpoint/removeUserByLoginResponse")
    @RequestWrapper(localName = "removeUserByLogin", targetNamespace = "http://endpoint.tm.pkonovalov.ru/", className = "ru.pkonovalov.tm.endpoint.RemoveUserByLogin")
    @ResponseWrapper(localName = "removeUserByLoginResponse", targetNamespace = "http://endpoint.tm.pkonovalov.ru/", className = "ru.pkonovalov.tm.endpoint.RemoveUserByLoginResponse")
    public void removeUserByLogin(

            @WebParam(name = "session", targetNamespace = "")
                    ru.pkonovalov.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.pkonovalov.ru/AdminUserEndpoint/unlockUserByLoginRequest", output = "http://endpoint.tm.pkonovalov.ru/AdminUserEndpoint/unlockUserByLoginResponse")
    @RequestWrapper(localName = "unlockUserByLogin", targetNamespace = "http://endpoint.tm.pkonovalov.ru/", className = "ru.pkonovalov.tm.endpoint.UnlockUserByLogin")
    @ResponseWrapper(localName = "unlockUserByLoginResponse", targetNamespace = "http://endpoint.tm.pkonovalov.ru/", className = "ru.pkonovalov.tm.endpoint.UnlockUserByLoginResponse")
    public void unlockUserByLogin(

            @WebParam(name = "session", targetNamespace = "")
                    ru.pkonovalov.tm.endpoint.Session session,
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login
    );
}
