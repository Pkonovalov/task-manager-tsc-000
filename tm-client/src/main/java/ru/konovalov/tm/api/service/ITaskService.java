package ru.konovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.IOwnerService;
import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IOwnerService<Task> {

    @Nullable
    List<Task> findAll(@NotNull String userId);

    List<Task> findAll(@NotNull String userId, Comparator<Task> comparator);

/*
    @NotNull
    Task add(@NotNull String userId, @NotNull Task task);
*/

    @NotNull
    Task add(@NotNull String userId, @Nullable String name, @Nullable String description);

    void remove(@NotNull String userId, @NotNull Task task);

    void clear(@NotNull String userId);

    void clear();

    void removeOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    Task findOneById(@NotNull String userId, @Nullable String id);

    @Nullable
    Task findOneByName(@NotNull String userId, @Nullable String name);

    void removeOneByName(@NotNull String userId, @NotNull String name);

    void removeTaskByIndex(@NotNull String userId, @NotNull Integer index);

    void finishTaskByIndex(@NotNull String userId, @NotNull Integer index);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    Task findOneByIndex(@NotNull String userId, @Nullable Integer index);

    Task finishTaskById(@NotNull String userId, @Nullable String id);

    @Nullable
    Task finishTaskByName(@NotNull String userId, @Nullable String name);

    @Nullable
    Task changeTaskStatusById(@NotNull String userId, @Nullable String id, @NotNull Status status);

    @Nullable
    Task changeTaskStatusByName(@NotNull String userId, @NotNull String name, @NotNull Status status);

    @Nullable
    Task changeTaskStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    String getIdByIndex(@NotNull String userId, @NotNull Integer index);

    int size(@NotNull String userId);

    boolean existsByName(@NotNull String userId, @NotNull String name);

    boolean existsById(@NotNull String userId, @NotNull String id);

    void setTaskStatusById(@NotNull String userId, @Nullable String id, @NotNull Status status);

    void setTaskStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    void setTaskStatusByName(@NotNull String userId, @Nullable String name, @NotNull Status status);

    void startTaskById(@NotNull String userId, @Nullable String id);

    void startTaskByIndex(@NotNull String userId, @NotNull Integer index);

    void startTaskByName(@NotNull String userId, @Nullable String name);

    void updateTaskById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    void updateTaskByIndex(@NotNull String userId, @NotNull Integer index, @Nullable String name, @Nullable String description);

    void updateTaskByName(@NotNull String userId, @Nullable String name, @Nullable String nameNew, @Nullable String description);

}
