package ru.konovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.AbstractCommand;
import ru.konovalov.tm.model.Command;

import java.util.Collection;

public interface ICommandService {

    void add(@NotNull AbstractCommand command);

    @Nullable
    Collection<AbstractCommand> getCommands();

    @Nullable
    Collection<AbstractCommand> getArguments();

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @Nullable
    AbstractCommand getCommandByArg(@Nullable String arg);

    @Nullable
    Collection<String> getListArgumentName();

    @Nullable
    Collection<String> getListCommandName();

}
