package ru.konovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.IService;
import ru.konovalov.tm.enumerated.Role;
import ru.konovalov.tm.model.User;


public interface IUserService extends IService<User> {

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull Role role);

    @Nullable
    User findById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeById(@NotNull String id);

    void removeByLogin(@Nullable String login);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    @NotNull
    User add(@NotNull String login, @NotNull String password);

    @NotNull
    User create(@NotNull String login, @NotNull String password, String email);

    void setPassword(@NotNull String id, @NotNull String password);

    @Nullable
    User updateUser(
            @NotNull String id,
            @NotNull String firstName,
            @NotNull String lastName,
            @NotNull String middleName
    );

    boolean existsByLogin(@NotNull String login);

    void lockUserByLogin(@NotNull String login);

    void unlockUserByLogin(@NotNull String login);

}
