package ru.konovalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.api.IRepository;
import ru.konovalov.tm.model.Session;

import java.util.List;

public interface ISessionRepository extends IRepository<Session> {

    boolean contains(@NotNull String id);

    List<Session> findByUserId(@NotNull String userId);

    void removeByUserId(@NotNull String userId);

}
