package ru.konovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.api.service.IPropertyService;

public interface ServiceLocator {

    @NotNull ITaskService getTaskService();

    @NotNull IProjectService getProjectService();

    @NotNull IProjectTaskService getProjectTaskService();

    @NotNull ICommandService getCommandService();

    @NotNull IAuthService getAuthService();

    @NotNull IUserService getUserService();

    @NotNull IPropertyService getPropertyService();

    @NotNull ISessionService getSessionService();
}
