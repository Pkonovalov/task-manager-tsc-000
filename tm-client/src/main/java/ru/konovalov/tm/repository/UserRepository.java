package ru.konovalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.repository.IUserRepository;
import ru.konovalov.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        return list.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);

    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return list.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null);

    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        return list.stream()
                .filter(e -> email.equals(e.getEmail()))
                .findFirst()
                .orElse(null);

    }

    @Nullable
    @Override
    public User removeUser(@NotNull final User user) {
        list.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeUserById(@NotNull final String id) {
        final User user = findById(id);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeUserByLogin(@NotNull final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return list.stream()
                .anyMatch(e -> id.equals(e.getId()));
    }

    @Override
    public boolean existsByLogin(@NotNull final String login) {
        return list.stream()
                .anyMatch(e -> login.equals(e.getLogin()));

    }

    public boolean existsByEmail(@NotNull final String email) {
        return list.stream()
                .anyMatch(e -> email.equals(e.getEmail()));

    }

    @Override
    public void setPasswordById(@NotNull final String id, @NotNull final String passwordHash) {
        @Nullable final User user = findById(id);
        if (user == null) return;
        user.setPasswordHash(passwordHash);

    }
}
