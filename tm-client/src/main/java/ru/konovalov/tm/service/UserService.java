package ru.konovalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.repository.IUserRepository;
import ru.konovalov.tm.api.service.IPropertyService;
import ru.konovalov.tm.api.service.IUserService;
import ru.konovalov.tm.enumerated.Role;
import ru.konovalov.tm.exeption.empty.EmptyEmailException;
import ru.konovalov.tm.exeption.empty.EmptyIdException;
import ru.konovalov.tm.exeption.empty.EmptyLoginException;
import ru.konovalov.tm.exeption.empty.EmptyPasswordException;
import ru.konovalov.tm.exeption.entity.LoginExistException;
import ru.konovalov.tm.exeption.entity.UserNotFoundException;
import ru.konovalov.tm.exeption.user.EmailExistsException;
import ru.konovalov.tm.exeption.user.LoginExistsException;
import ru.konovalov.tm.model.User;
import ru.konovalov.tm.util.HashUtil;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;
    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository, @NotNull final IPropertyService propertyService) {
        super(userRepository);
        this.userRepository = userRepository;
        this.propertyService = propertyService;
    }

    @Nullable
    @Override
    public User findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Nullable
    @Override
    public User removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeUserById(id);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        userRepository.removeUserByLogin(login);
    }

    @Override
    public boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @NotNull
    @Override
    public User add(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExist(login)) throw new LoginExistException(login);
        @NotNull final User user = new User();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (userRepository.existsByLogin(login)) throw new LoginExistsException();
        if (userRepository.existsByEmail(email)) throw new EmailExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        userRepository.add(user);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @NotNull final Role role) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (userRepository.existsByLogin(login)) throw new LoginExistsException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        userRepository.add(user);
        return user;
    }


    @Override
    public void setPassword(@NotNull final String userId, @Nullable final String password) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (!userRepository.existsById(userId)) throw new UserNotFoundException();
        userRepository.setPasswordById(userId, HashUtil.salt(propertyService, password));
    }

    @Override
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final User user = findById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    public boolean existsByLogin(@Nullable final String login) {
        return userRepository.existsByLogin(login);
    }

    public void lockUserByLogin(@Nullable String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
    }

    public void unlockUserByLogin(@Nullable String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
    }

}
