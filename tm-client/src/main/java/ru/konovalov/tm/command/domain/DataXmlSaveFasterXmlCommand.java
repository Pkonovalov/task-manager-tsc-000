package ru.konovalov.tm.command.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.dto.Domain;

import java.io.FileOutputStream;

public final class DataXmlSaveFasterXmlCommand extends AbstractDomainCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return "Save data to xml file";
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-save";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML SAVE]");
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_FASTERXML_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}