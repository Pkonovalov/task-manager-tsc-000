package ru.konovalov.tm.command.user;

import ru.konovalov.tm.command.AbstractCommand;
import ru.konovalov.tm.exeption.entity.UserNotFoundException;
import ru.konovalov.tm.model.User;

abstract class AbstractUserCommand extends AbstractCommand {

    protected void showUser(final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
