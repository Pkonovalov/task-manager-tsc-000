package ru.konovalov.tm;

import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.konovalov.tm.marker.UnitCategory;

import static org.junit.Assert.assertTrue;

/**
 * Unit test for simple App.
 */
public class AppTest {

    @Test
    @Category(UnitCategory.class)
    public void test() {
        Assert.assertNotNull(App.class);
    }

}