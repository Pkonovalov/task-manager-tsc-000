package ru.konovalov.tm.endpointtest;

import com.sun.xml.internal.ws.fault.ServerSOAPFaultException;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.konovalov.tm.marker.SoapCategory;
import ru.konovalov.tm.model.Session;
import ru.konovalov.tm.model.User;

public class SessionEndpointTest extends AbstractEndpointTest {

    @Test(expected = ServerSOAPFaultException.class)
    @Category(SoapCategory.class)
    public void testCloseSession() {
        @NotNull final Session session = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        Assert.assertNotNull(session);
        @NotNull final User user = SESSION_ENDPOINT.getUser(session);
        Assert.assertNotNull(user);
        Assert.assertEquals(TEST_USER_NAME, user.getLogin());
        SESSION_ENDPOINT.closeSession(session);
        Assert.assertNull(SESSION_ENDPOINT.getUser(session));
    }

    @Test
    @Category(SoapCategory.class)
    public void testIncorrect() {
        @NotNull final Session session = SESSION_ENDPOINT.openSession("qweqwe", "123123");
        Assert.assertNull(session);
    }

    @Test
    @Category(SoapCategory.class)
    public void testOpenSession() {
        @NotNull final Session session = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);
        Assert.assertNotNull(session);
        @NotNull final User user = SESSION_ENDPOINT.getUser(session);
        Assert.assertNotNull(user);
        Assert.assertEquals(TEST_USER_NAME, user.getLogin());
    }

}
