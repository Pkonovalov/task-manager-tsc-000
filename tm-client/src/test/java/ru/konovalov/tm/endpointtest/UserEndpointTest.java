package ru.konovalov.tm.endpointtest;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.konovalov.tm.endpoint.UserEndpoint;
import ru.konovalov.tm.marker.SoapCategory;

public class UserEndpointTest extends AbstractEndpointTest {

    @NotNull
    final static String FIRST_NAME = "Petr";
    @NotNull
    final static String LAST_NAME = "K";
    @NotNull
    final static String MID_NAME = "Michailovich";
    @NotNull
    final static String SALT = "qweqwe123";
    @NotNull
    final static UserEndpoint USER_ENDPOINT = (UserEndpoint) BOOTSTRAP.getUserEndpoint();

    @Test
    @Category(SoapCategory.class)
    public void updatePassword() {
        USER_ENDPOINT.updateUserPassword(SESSION, SALT + TEST_USER_PASSWORD + SALT);
        SESSION_ENDPOINT.closeSession(SESSION);
        SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, SALT + TEST_USER_PASSWORD + SALT);
        Assert.assertNotNull(SESSION);
        USER_ENDPOINT.updateUserPassword(SESSION, TEST_USER_PASSWORD);
    }

    @Test
    @Category(SoapCategory.class)
    public void updateUser() {
        USER_ENDPOINT.updateUser(SESSION, FIRST_NAME, LAST_NAME, MID_NAME);
        Assert.assertEquals(FIRST_NAME, USER_ENDPOINT.viewUser(SESSION).getFirstName());
        Assert.assertEquals(LAST_NAME, USER_ENDPOINT.viewUser(SESSION).getLastName());
        Assert.assertEquals(MID_NAME, USER_ENDPOINT.viewUser(SESSION).getMiddleName());
    }

}
