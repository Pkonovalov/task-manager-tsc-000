package ru.konovalov.tm.endpointtest;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.bootstrap.Bootstrap;
import ru.konovalov.tm.endpoint.SessionEndpoint;
import ru.konovalov.tm.model.Session;

public abstract class AbstractEndpointTest {

    @NotNull
    protected static final String ADMIN_USER_NAME = "admin";
    @NotNull
    protected static final String ADMIN_USER_PASSWORD = "admin";
    @NotNull
    protected static final Bootstrap BOOTSTRAP = new Bootstrap();
    @NotNull
    protected static final SessionEndpoint SESSION_ENDPOINT = BOOTSTRAP.getSessionEndpoint();
    @NotNull
    protected static final String TEST_USER_NAME = "test";
    @NotNull
    protected static final String TEST_USER_PASSWORD = "test";
    @NotNull
    protected static Session SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);

}
