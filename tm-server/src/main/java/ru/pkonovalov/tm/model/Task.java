package ru.pkonovalov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.entity.IWBS;
import ru.pkonovalov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractEntity implements IWBS {

    @Nullable
    private Date created = new Date();

    @Nullable
    private Date dateFinish;

    @Nullable
    private Date dateStart;

    @Nullable
    private String description;

    @Nullable
    private String name;

    @Nullable
    private String projectId = null;

    @Nullable
    private Status status = Status.NOT_STARTED;

    @Nullable
    public Task(@Nullable final String name) {
        this.name = name;
    }

    public void setStatus(@Nullable final Status status) {
        if (status == null) return;
        this.status = status;
        switch (status) {
            case IN_PROGRESS:
                this.setDateStart(new Date());
                break;
            case COMPLETE:
                this.setDateFinish(new Date());
            default:
                break;
        }
    }

    @NotNull
    @Override
    public String toString() {
        return String.format("| %s | %-12s | %-20s | %-30s | %-30s | %-30s | %s ", getId(), status, name, created, dateStart, dateFinish, projectId);
    }

}
