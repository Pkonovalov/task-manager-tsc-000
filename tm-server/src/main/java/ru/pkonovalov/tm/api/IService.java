package ru.pkonovalov.tm.api;

import ru.pkonovalov.tm.model.AbstractEntity;

public interface IService<E extends AbstractEntity> extends IRepository<E> {

}
