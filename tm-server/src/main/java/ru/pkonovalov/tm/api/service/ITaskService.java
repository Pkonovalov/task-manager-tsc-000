package ru.pkonovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.IService;
import ru.pkonovalov.tm.enumerated.Status;
import ru.pkonovalov.tm.model.Task;

public interface ITaskService extends IService<Task> {

    @NotNull
    Task add(@NotNull String userId, @Nullable String name, @Nullable String description);

    boolean existsByName(@NotNull String userId, @Nullable String name);

    @Nullable
    Task findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Task findOneByName(@NotNull String userId, @Nullable String name);

    void finishTaskById(@NotNull String userId, @Nullable String id);

    void finishTaskByIndex(@NotNull String userId, @NotNull Integer index);

    void finishTaskByName(@NotNull String userId, @Nullable String name);

    @Nullable
    String getIdByIndex(@NotNull String userId, @NotNull Integer index);

    void removeOneById(@NotNull String userId, @Nullable String id);

    void removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    void removeOneByName(@NotNull String userId, @Nullable String name);

    void setTaskStatusById(@NotNull String userId, @Nullable String id, @NotNull Status status);

    void setTaskStatusByIndex(@NotNull String userId, @NotNull Integer index, @NotNull Status status);

    void setTaskStatusByName(@NotNull String userId, @Nullable String name, @NotNull Status status);

    void startTaskById(@NotNull String userId, @Nullable String id);

    void startTaskByIndex(@NotNull String userId, @NotNull Integer index);

    void startTaskByName(@NotNull String userId, @Nullable String name);

    void updateTaskById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    void updateTaskByIndex(@NotNull String userId, @NotNull Integer index, @Nullable String name, @Nullable String description);

    void updateTaskByName(@NotNull String userId, @Nullable String name, @Nullable String nameNew, @Nullable String description);

}
