package ru.pkonovalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.model.Session;
import ru.pkonovalov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @WebMethod
    boolean existsUserByEmail(
            @WebParam(name = "email", partName = "email") @NotNull String email
    );

    @WebMethod
    boolean existsUserByLogin(
            @WebParam(name = "login", partName = "login") @NotNull String login
    );

    @WebMethod
    void registryUser(
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password,
            @WebParam(name = "email", partName = "email") @NotNull String email
    );

    @WebMethod
    void updateUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "firstName", partName = "firstName") @NotNull String firstName,
            @WebParam(name = "lastName", partName = "lastName") @NotNull String lastName,
            @WebParam(name = "middleName", partName = "middleName") @NotNull String middleName
    );

    @WebMethod
    void updateUserPassword(
            @WebParam(name = "session", partName = "session") @NotNull Session session,
            @WebParam(name = "password", partName = "password") @NotNull String password
    );

    @Nullable
    @WebMethod
    User viewUser(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

}
