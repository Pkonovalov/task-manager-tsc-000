package ru.pkonovalov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.IUserRepository;
import ru.pkonovalov.tm.constant.FieldConst;
import ru.pkonovalov.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @SneakyThrows
    public void update(@Nullable final User user) {
        if (user == null) return;
        @NotNull final String query =
                "UPDATE `app_user` " +
                        "SET `"+ FieldConst.LOGIN+"` = ?, `"+ FieldConst.PASSWORD_HASH+"` = ?, `"+ FieldConst.USER_ID+"` = ?, `"+ FieldConst.EMAIL+"` = ? " +
                        "WHERE `id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, user.getLogin());
        statement.setString(2, user.getPasswordHash());
        statement.setString(3, user.getId());
        statement.setString(4, user.getEmail());
        statement.execute();
    }

    @Override
    public boolean existsByEmail(@Nullable final String email) {
        if (email == null) return false;
        return list.stream()
                .anyMatch(e -> email.equals(e.getEmail()));
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return list.stream()
                .anyMatch(e -> id.equals(e.getId()));
    }

    @Override
    public boolean existsByLogin(@Nullable final String login) {
        if (login == null) return false;
        return list.stream()
                .anyMatch(e -> login.equals(e.getLogin()));
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        return list.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        remove(list.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null)
        );
    }

    @Override
    public void setPasswordById(@NotNull final String id, @NotNull final String passwordHash) {
        @Nullable final User user = findById(id);
        if (user == null) return;
        user.setPasswordHash(passwordHash);
    }

}
