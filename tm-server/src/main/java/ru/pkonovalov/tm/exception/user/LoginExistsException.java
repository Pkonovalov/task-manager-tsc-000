package ru.pkonovalov.tm.exception.user;

import ru.pkonovalov.tm.exception.AbstractException;

public class LoginExistsException extends AbstractException {

    public LoginExistsException() {
        super("Error! Login already exists...");
    }

}
