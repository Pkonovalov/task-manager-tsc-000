package ru.pkonovalov.tm.exception.system;

import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(@Nullable final String command) {
        super("Unknown command ```" + command + "``. Type help to see all available commands...");
    }

}
