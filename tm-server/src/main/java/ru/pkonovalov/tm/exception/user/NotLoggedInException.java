package ru.pkonovalov.tm.exception.user;

import ru.pkonovalov.tm.exception.AbstractException;

public class NotLoggedInException extends AbstractException {

    public NotLoggedInException() {
        super("Error! You are not logged in...");
    }

}
