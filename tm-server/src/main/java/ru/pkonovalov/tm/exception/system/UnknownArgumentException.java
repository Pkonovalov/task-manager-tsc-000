package ru.pkonovalov.tm.exception.system;

import ru.pkonovalov.tm.exception.AbstractException;

public class UnknownArgumentException extends AbstractException {

    public UnknownArgumentException() {
        super("Error! Argument not found...");
    }

}
