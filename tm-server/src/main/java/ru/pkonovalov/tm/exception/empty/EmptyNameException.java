package ru.pkonovalov.tm.exception.empty;

import ru.pkonovalov.tm.exception.AbstractException;

public class EmptyNameException extends AbstractException {

    public EmptyNameException() {
        super("Error! Name is empty...");
    }

}
