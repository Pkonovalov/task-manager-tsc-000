package ru.pkonovalov.tm.util;

import org.jetbrains.annotations.NotNull;
import ru.pkonovalov.tm.exception.system.IndexIncorrectException;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String dashedLine() {
        @NotNull final StringBuilder sb = new StringBuilder(20);
        for (@NotNull int n = 0; n < 20; ++n)
            sb.append('-');
        sb.append(System.lineSeparator());
        return sb.toString();
    }

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @NotNull final String value = nextLine();
        try {
            return Integer.parseInt(value);
        } catch (@NotNull final Exception e) {
            throw new IndexIncorrectException(value);
        }
    }

}
