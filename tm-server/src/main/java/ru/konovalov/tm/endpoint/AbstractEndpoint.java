package ru.konovalov.tm.endpoint;

import ru.konovalov.tm.api.service.ServiceLocator;

public class AbstractEndpoint {

    protected final ServiceLocator serviceLocator;

    AbstractEndpoint(final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

}
