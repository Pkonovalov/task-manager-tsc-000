package ru.konovalov.tm.bootstrap;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.konovalov.tm.api.endpoint.*;
import ru.konovalov.tm.api.repository.*;
import ru.konovalov.tm.api.service.*;
import ru.konovalov.tm.command.AbstractCommand;
import ru.konovalov.tm.component.Backup;
import ru.konovalov.tm.component.FileScanner;
import ru.konovalov.tm.endpoint.*;
import ru.konovalov.tm.enumerated.Role;
import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.exeption.system.UnknownCommandException;
import ru.konovalov.tm.repository.*;
import ru.konovalov.tm.service.*;
import ru.konovalov.tm.util.SystemUtil;
import ru.konovalov.tm.util.TerminalUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.Set;

@Getter
public class Bootstrap implements ServiceLocator {

    @NotNull
    private final IAdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint(this);
    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);
    @NotNull
    private final ILoggerService loggerService = new LoggerService();
    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);
    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final ConnectionProvider connectionProvider = (ConnectionProvider) this;
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @Nullable
    private Connection connection = connectionService.getConnection();
    @NotNull
    private final Backup backup = new Backup(this, propertyService);
    @NotNull
    private final IAdminEndpoint adminEndpoint = new AdminEndpoint(this, backup);
    @NotNull
    private final FileScanner fileScanner = new FileScanner(this, propertyService);
    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);
    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository, this);
    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);
    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository, authService);
    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);
    @NotNull
    private final IUserRepository userRepository = new UserRepository(connection);
    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);
    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    private void init() {
        initCommands();
        initPID();
        backup.init();
        fileScanner.init();
    }

    @SneakyThrows
    public void initPID() {
        final String fileName = "task-manager.pid";
        final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(fileName), pid.getBytes());
        final File file = new File(fileName);
        file.deleteOnExit();
    }

    private void initEndpoint() {
        registry(sessionEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(adminEndpoint);
        registry(adminUserEndpoint);
    }

    @SneakyThrows
    private void initCommands() {
        @NotNull final Reflections reflections = new Reflections("ru.konovalov.tm.command");
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            final boolean isAbstract = Modifier.isAbstract(clazz.getModifiers());
            if (isAbstract) continue;
            registry(clazz.newInstance());
        }
    }

    public void run(@Nullable final String[] args) {
        loggerService.info("*** WeLcOmE To TaSk MaNaGeR ***");
        if (parseArgs(args)) System.exit(0);
        init();
        while (true) {
            System.out.println("ENTER COMMAND:");
            @Nullable final String command = TerminalUtil.nextLine();
            loggerService.command(command);
            try {
                parseCommand(command);
                System.out.println("[OK]");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.err.println("[FAILED]");
            }
        }
    }

    private void registry(@Nullable final Object endpoint) {
        if (endpoint == null) return;
        @NotNull final String host = propertyService.getServerHost();
        final int port = propertyService.getServerPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + "/" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    private void initData() {
        @NotNull final String idTest = userService.create("test", "test", "test@test.ru").getId();
        @NotNull final String idAdmin = userService.create("admin", "admin", Role.ADMIN).getId();

        projectService.add(idTest, "proj1", "desc1").setStatus(Status.COMPLETE);
        projectService.add(idTest, "proj2", "desc2").setStatus(Status.IN_PROGRESS);
        projectService.add(idTest, "proj3", "desc3").setStatus(Status.IN_PROGRESS);
        projectService.add(idAdmin, "proj4", "desc4").setStatus(Status.NOT_STARTED);
        projectService.add(idAdmin, "proj5", "desc5").setStatus(Status.COMPLETE);
        projectService.add(idAdmin, "proj6", "desc6").setStatus(Status.NOT_STARTED);

        taskService.add(idTest, "task1", "task_desc1").setStatus(Status.COMPLETE);
        taskService.add(idTest, "task2", "task_desc2").setStatus(Status.NOT_STARTED);
        taskService.add(idTest, "task3", "task_desc3").setStatus(Status.IN_PROGRESS);
        taskService.add(idAdmin, "task4", "task_desc4").setStatus(Status.NOT_STARTED);
        taskService.add(idAdmin, "task5", "task_desc5").setStatus(Status.IN_PROGRESS);
        taskService.add(idAdmin, "task6", "task_desc6").setStatus(Status.NOT_STARTED);
    }

    public void parseArg(@Nullable final String arg) {
        if (arg == null) return;
        @Nullable final AbstractCommand command = commandService.getCommandByArg(arg);
        if (command == null) return;
        command.execute();
    }


    public void parseCommand(@Nullable final String com) {
        if (com == null) return;
        final AbstractCommand command = commandService.getCommandByName(com);
        if (command == null) throw new UnknownCommandException(com);
        @Nullable final Role[] roles = command.roles();
        authService.checkRoles(roles);
        command.execute();
    }

    public boolean parseArgs(@Nullable String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }


    @Override
    public @NotNull ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public @NotNull IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public @NotNull IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public @NotNull ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public @NotNull IAuthService getAuthService() {
        return authService;
    }

    @Override
    public @NotNull IUserService getUserService() {
        return userService;
    }
}