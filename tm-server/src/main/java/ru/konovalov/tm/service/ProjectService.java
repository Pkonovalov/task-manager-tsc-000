package ru.konovalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.repository.IProjectRepository;
import ru.konovalov.tm.api.service.IProjectService;
import ru.konovalov.tm.enumerated.Status;
import ru.konovalov.tm.exeption.empty.EmptyIdException;
import ru.konovalov.tm.exeption.empty.EmptyIndexException;
import ru.konovalov.tm.exeption.empty.EmptyNameException;
import ru.konovalov.tm.exeption.entity.ProjectNotFoundException;
import ru.konovalov.tm.exeption.system.IndexIncorrectException;
import ru.konovalov.tm.model.Project;

import java.util.Date;

import static ru.konovalov.tm.util.ValidationUtil.checkIndex;
import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository) {
        super(projectRepository, authService);
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    public Project findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectRepository.findById(userId, id);
        if (project == null) throw new EmptyIdException();
        return project;
    }

    @NotNull
    @Override
    public Project add(@NotNull final String userId, @Nullable final String name, @Nullable final String description) {
        if (isEmpty(name)) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @NotNull
    @Override
    public Project removeOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectRepository.removeById(userId, id);
        if (project == null) throw new EmptyIdException();
        return project;
    }

    @NotNull
    @Override
    public Project findOneByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = projectRepository.findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @NotNull
    @Override
    public Project removeOneByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = projectRepository.removeOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        return project;
    }

    @Nullable
    @Override
    public Project removeProjectByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return projectRepository.removeOneByIndex(userId, index);
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        if (index > projectRepository.size(userId)) throw new IndexIncorrectException();
        return projectRepository.findOneByIndex(userId, index);
    }

    @NotNull
    @Override
    public Project updateProjectByIndex(@NotNull final String userId, @NotNull final Integer index, @Nullable final String name, @Nullable final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = projectRepository.findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project updateProjectById(@NotNull final String userId, @Nullable final String id, @Nullable final String name, @NotNull final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findById(userId, id);
        assert project != null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project updateProjectByName(@NotNull final String userId, @Nullable final String name, @NotNull final String nameNew, @Nullable final String description) {
        if (isEmpty(name) || isEmpty(nameNew)) throw new EmptyNameException();
        @NotNull final Project project = findOneByName(userId, name);
        project.setName(nameNew);
        project.setDescription(description);
        return project;
    }

    @NotNull
    @Override
    public Project startProjectById(@NotNull String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectRepository.findById(userId, id);
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project startProjectByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @Nullable final Project project = projectRepository.findOneByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;

    }

    @Nullable
    @Override
    public Project startProjectByName(@NotNull String userId, @Nullable String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final Project project = projectRepository.findOneByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setDateStart(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project finishProjectById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectRepository.findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateFinish(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project finishProjectByIndex(@NotNull final String userId, @Nullable Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Project project = projectRepository.findOneByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateFinish(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project finishProjectByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = projectRepository.findOneByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        project.setDateFinish(new Date());
        return project;
    }

    @Nullable
    @Override
    public Project changeProjectStatusById(@NotNull final String userId, @Nullable final String id, @Nullable final Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final Project project = projectRepository.findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Nullable
    @Override
    public Project changeProjectStatusByName(@NotNull final String userId, @Nullable final String name, @Nullable final Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final @Nullable Project project = findOneByName(userId, name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Nullable
    @Override
    public Project changeProjectStatusByIndex(@NotNull final String userId, @Nullable final Integer index, @NotNull final Status status) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public boolean existsByName(@NotNull String userId, @Nullable String name) {
        if (isEmpty(name)) throw new EmptyNameException();
        return projectRepository.existsByName(userId, name);
    }

    @Override
    public void setProjectStatusById(@NotNull final String userId, @Nullable final String id, @NotNull final Status status) {
        if (isEmpty(id)) throw new EmptyIdException();
        @Nullable final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
    }

    @Override
    public void setProjectStatusByIndex(@NotNull final String userId, @NotNull final Integer index, @NotNull final Status status) {
        if (!checkIndex(index, projectRepository.size(userId))) throw new IndexIncorrectException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
    }

    @Override
    public void setProjectStatusByName(@NotNull final String userId, @Nullable final String name, @NotNull final Status status) {
        if (isEmpty(name)) throw new EmptyNameException();
        @Nullable final Project project = findOneByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
    }

}

