package ru.konovalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.IOwnerRepository;
import ru.konovalov.tm.model.AbstractOwner;
import ru.konovalov.tm.model.Project;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwner> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @NotNull
    protected final List<E> list = new ArrayList<>();

    public AbstractOwnerRepository(Connection connection) {

    }

    @Nullable
    public List<E> findAll(@NotNull final String userId) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public E findById(@NotNull final String userId, @NotNull final String id) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .findFirst().orElse(null);
    }

    @Override
    public int size(@NotNull String userId) {
        int i = 0;
        for (final E e : list) {
            if (userId.equals(e.getUserId())) i++;
        }
        return i;
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        for (final E e : list) {
            if (id.equals(e.getId()) && userId.equals(e.getUserId())) return true;
        }
        return false;
    }

    @Override
    public void clear(@NotNull final String userId) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .forEach(list::remove);

    }

    @Nullable
    @Override
    public Project removeById(@NotNull final String userId, @NotNull final String id) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst()
                .ifPresent(this::remove);
        return null;
    }


}