package ru.konovalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.IOwnerRepository;
import ru.konovalov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IOwnerRepository<Task> {

/*    List<Task> findAll(final @NotNull String userId, final Comparator<Task> comparator);

    List<Task> findAll(@NotNull String userId);*/

    List<Task> findALLTaskByProjectId(@NotNull String userId, String projectId);

    List<Task> removeAllTaskByProjectId(final @NotNull String userId, final String projectId);

    @Nullable
    Task assignTaskByProjectId(@NotNull String userId, @NotNull String projectId, @NotNull String taskId);

    @Nullable
    Task unassignTaskByProjectId(@NotNull String userId, @NotNull String taskId);

    void add(@NotNull String userId, Task task);

    void remove(@NotNull String userId, Task task);

    void clear(@NotNull String userId);

    @Nullable
    String getIdByIndex(@NotNull int index);

    @Nullable
    Task findOneById(String userId, String id);

    @Nullable
    Task removeOneById(String userId, String id);

    @Nullable
    Task findOneByIndex(String userId, Integer index);

    @Nullable
    Task removeOneByIndex(String userId, Integer index);

    @Nullable
    Task findOneByName(String userId, String name);

    @Nullable
    Task removeOneByName(String userId, String name);

    void removeAllByProjectId(@NotNull String userId, String projectId);

    int size(@NotNull String userId);

    boolean existsById(@NotNull String userId, String projectId);

    boolean existsByName(@NotNull String userId, @NotNull String name);

    void removeAllBinded(@NotNull String userId);

    void bindTaskPyProjectId(String testUserId, String projectId, String testTaskId);

    boolean existsByProjectId(String testUserId, String projectId);

    void unbindTaskFromProject(String testUserId, String testTaskId);
}
