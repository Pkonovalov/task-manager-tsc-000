package ru.konovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.IService;
import ru.konovalov.tm.enumerated.Role;
import ru.konovalov.tm.model.Session;
import ru.konovalov.tm.model.User;

import java.util.List;

public interface ISessionService extends IService<Session> {

    boolean checkDataAccess(@Nullable String login, @Nullable String password);

    void close(@NotNull Session session);

    void closeAll(@NotNull Session session);

    @Nullable List<Session> getListSession(@NotNull Session session);

    @Nullable User getUser(@NotNull Session session);

    @NotNull String getUserId(@NotNull Session session);

    boolean isValid(@NotNull Session session);

    @Nullable Session open(@Nullable String login, @Nullable String password);

    @Nullable Session sign(@Nullable Session session);

    void signOutByLogin(@Nullable String login);

    void signOutByUserId(@Nullable String userId);

    void validate(@NotNull Session session, @Nullable Role role);

    void validate(@Nullable Session session);
}
