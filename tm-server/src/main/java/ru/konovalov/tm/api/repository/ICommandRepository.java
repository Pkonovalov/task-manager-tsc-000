package ru.konovalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(@NotNull AbstractCommand command);

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    @Nullable
    AbstractCommand getCommandByArg(@NotNull String name);

    @NotNull
    Collection<String> getCommandNames();

    @NotNull
    Collection<String> getCommandArgs();

    @Nullable
    AbstractCommand getCommandByName(@NotNull String name);

}
